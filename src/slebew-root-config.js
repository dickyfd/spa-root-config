import { registerApplication, start } from "single-spa";

// registerApplication({
//   name: "@single-spa/welcome",
//   app: () =>
//     System.import(
//       "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
//     ),
//   activeWhen: ["/"],
// });

registerApplication({
  name: "@slebew/spa-home",
  app: () => System.import("@slebew/spa-home"),
  activeWhen: ["/"]
});

start({
  urlRerouteOnly: true,
});
